import requests, click, json
IP = "151.217.83.38"
url = "http://"+IP+":8080/"


def f_set(state):
    return json.loads(requests.post(url+'api', data=json.dumps(state)).content)

@click.command()
@click.option('-t', '--toggle', help='Toggle LED status.', is_flag=True)
@click.option('-b', '--brightness', type=int, help='Set brightness: 0-255')
@click.option('-m', '--mode', type=int, help='Set mode: 0 (static) or 1 (animation))')
@click.option('-g', '--gradient', type=int, help='Set color gradient: 0 - x')
@click.option('-c', '--color', help='Set static color: {r},{g},{b}')
@click.option('-s', '--speed', type=int, help='Set speed: 0-255')

def main(toggle, brightness, mode, gradient, color, speed):
    """Stripper-Klieh"""
    data = {}
    if toggle:
        if f_set({})["on"] == False:
            data["on"] = True
        else:
            data["on"] = False
    if brightness is not None:
        data["brightness"] = brightness
    if mode is not None:
        data["mode"] = mode
    if gradient is not None:
        data["gradient"] = gradient
    if color:
        values = color.split(",")
        data["gradient"] = -1
        data["color"] = [int(values[0]), int(values[1]), int(values[2])]
    if speed is not None:
        data["speed"] = speed
    print(f_set(data))
if __name__=="__main__":
    main()
